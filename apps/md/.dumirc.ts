import { defineConfig } from 'dumi';

const BaseUrl = process.env.NODE_ENV === 'production' ? '/doc/' : '/'
const year = new Date().getFullYear()
const Domain = '<a href="https://react-admin.cn">react-admin.cn</a>'

export default defineConfig({
  favicons: ['/images/react.svg'],
  base: BaseUrl,
  publicPath: BaseUrl, // 打包文件时，引入地址生成 publicPath/xxx.js
  themeConfig: {
    name: '',
    footer: `${Domain} © ${year}. All rights reserved.`,
  },
});

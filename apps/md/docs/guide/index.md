---
title: 介绍
order: 1
toc: menu
nav:
  title: 指南
  order: 1
---

## 什么是 dumi？

dumi，中文发音**嘟米**，是一款为组件开发场景而生的文档工具，与 [father](https://github.com/umijs/father) 一起为开发者提供一站式的组件开发体验，**father 负责构建，而 dumi 负责组件开发及组件文档生成**。

## 特性

- 📦 开箱即用，将注意力集中在组件开发和文档编写上
- 📋 丰富的 Markdown 扩展，不止于渲染组件 demo
- 🏷 基于 TypeScript 类型定义，自动生成组件 API
- 🎨 主题轻松自定义，还可创建自己的 Markdown 组件
- 📱 支持移动端组件库研发，内置移动端高清渲染方案
- 📡 一行命令将组件资产数据化，与下游生产力工具串联

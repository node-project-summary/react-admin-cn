---
title: 配置项
order: 1
toc: menu
nav:
  title: 配置项
  order: 3
---

# 配置项

在项目根目录创建 `.umirc.ts` 或 `config/config.ts` 文件，都可对 dumi 进行配置：

```ts
// 配置文件
export default {
  // 具体配置项
};
```

目前 dumi 支持以下配置项。

## 基础配置

### algolia

- 类型: `Object`
- 默认值：`null`
- 详细：

配置 Algolia 的 [DocSearch](https://docsearch.algolia.com/) 服务，通常你会需要启用 sitemap.xml 的自动生成，以便顺利接入 Algolia，参考 [配置项 - sitemap](#sitemap)。

示例：

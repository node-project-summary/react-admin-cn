/* eslint-disable max-len */
import pkg from '../package.json'
import { Empty } from 'antd'

// 存储key
export const StoreKey = 'react-admin-cn'
export const TokenKey = `${StoreKey}-Token`
export const CITY_CODE = process.env.NEXT_PUBLIC_CITY_CODE || ''

// 分页配置
export const PageConfig: any = {
  base: { current: 1, page: 1, limit: 8 },
  options: { showQuickJumper: true, current: 1, page: 1, showSizeChanger: true, pageSizeOptions: ['5', '10', '20', '30', '50', '100'] },
}

// 基础配置
export default {
  name: pkg.name,
  version: pkg.version,
  title: 'React-Admin',
  author: 'Admin',
  keywords: 'react前端交流群 - 530415177',
  description: 'react前端,react后台管理模板,react管理模板',
}

export const ROUTES_LINKS = [
  { id: 1, name: 'admin', title: '首页', path: '/' },
  { id: 2, name: 'ui', title: 'Admin', path: '/admin' },
  { id: 3, name: 'hooks', title: 'UI组件库', path: '/ui' },
  { id: 4, name: 'cli', title: 'Hooks', path: '/hooks' },
  { id: 5, name: 'uniapp', title: 'CLI', path: '/cli' },
  { id: 6, name: 'tool', title: '关于我们', path: '/help/about/', isBlank: false },
  // { title: '帮助中心', path: '/docs', isBlank: true },
]

interface ICodeMessage {
  [propName: number]: string
}

export const CodeMessage: ICodeMessage = {
  200: '服务器成功返回请求的数据',
  201: '新建或修改数据成功',
  202: '一个请求已经进入后台排队（异步任务）',
  204: '删除数据成功',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作',
  401: '用户没有权限（令牌、用户名、密码错误）',
  403: '用户得到授权，但是访问是被禁止的',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作',
  405: '请求方法不被允许',
  406: '请求的格式不可得',
  410: '请求的资源被永久删除，且不会再得到的',
  422: '当创建一个对象时，发生一个验证错误',
  500: '服务器发生错误，请检查服务器',
  502: '网关错误',
  503: '服务不可用，服务器暂时过载或维护',
  504: '网关超时',
}
// 域名
export const Domain = 'react-admin.cn'
// APPID
export const App_Id = 2
// 标签颜色
export const TagColorList = ['red', 'orangered', 'orange', 'deeppink', 'lightcoral', 'green', 'cornflowerblue', 'blue', 'darkslategrey', 'purple', 'pinkpurple', 'magenta', 'gray', 'darkolivegreen', 'lightseagreen']
// 默认图片
export const Fallback = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=='
export const Favicon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAHX0lEQVR4Xu1aXW7bRhD+llSBAjZQ9QS1H9vIiHKC2ojU17gniHyCqiewfYIqJ4h8giqvlQI7J4gMK+lj5BNEARKgQCVOMaREcn+4u6RkRIXNR3u1O/vt/H4zAvf8E/f8/ngA4EED7jkCDyawMQW4/HsPUfQcEdURiBmIpohwjV8a47XO+GvSRIDHEGIv3TsILnD043StfZc/Xl8DLj/Usfj8EhDHBQJdAeij1bgoJfBo8hxAB8Ch+Xc0QLh7gqP9Wal9lcXrAcCXn3+5hEDTKQRhDMKJUyP4xQVeeu9Z2zlaB4T1ABi96wH0m/Py+QVCnOHpo3Pjb16/OwXRWan9IF6g9ahb7jfZ6uoAsM0vFh+Ug69B6ANUBwS/5DOjYKwN+ZdzaRLhFUBjQMwgYrN4LO0b7nxfVQuqAzCcdCHwR06QW7Qae5JgCUj8omzP8rcyCf5rscpfIAzPJIcX+5wv7Fh/SDck/I52o1dFC9YBYCC9sE2I4c0hhOhLQsfS0tKBiboi/C2IOmgfsAPVPx38C7QarBmlvzUAuLmCED9nr0BHhQLzokTN+4VmsdqI1b2207GqdALoZe7sN2gfFEQLOybVARjdfARyL9dq+O01mrAm6CYRy1nCoY0mlLvaFK3Gfunnj62v6icLAPgCkOQNHyTwVuYQ7u57O7Oq5yv3XQcAzsQyR+QDgNvby9Gh6HESR/jx65rAsKQPYGlHk5fL7M6md5w1nlgVcyt8QFkAdM/Nd1ylx7JPcIU1DQC8QrtRlIpbsaxuAsObMwhxmlPDc7QPzFlcUtC8VSS5RquRpNCjCcd1ObmJ8KQwbS5ztsPHVQfg9aQDAqt08nH44ldg+6R/HiOaNyGCOijiSx7qTs/lfeMc4QoiGIOiGYLaGOLb69hJDidyDiJwgqcNji6lv+oAaK/KAgsWWs4GS4vk/MF0mWpnyVMY7lctj6sBMHz3DILY5iplX84rll/QB4kB2o9elf2pPwBJXn8K8MW11LXsuXe0nmYg0UctfOGrEW4A0ot7vjbRJwDs1JoQ4jvppjbHtlpocpi2PYuh7CMMz11A2AGoUp+zPf67qGten6g4SqiXUL08/5/B+yacGUpwhzZRF62DF0WLzAAkmdafxXQUbkHooRYOMF/0tKow4QHyxcktwp2md5prKnk5InCkyZfgceEUdjFfHEOASZEsM5VuXEyfmQEYTt4aKSmiNxBhD62fBun+ajiMS1zVRwS/Sr8ZvT8GRafpGcwNiOBcW4OIHyH3KXur4S/ed9GVqtQsTI/RbjxRNUEHwExz3QJBVxJwtVOc33+eavaeHkxyqWpPh+U0WM02JSzoE2q7e0atYiAQMUGiaIRebcoAFNFc4c6hVX1tJS7leIJEMOVVlTfJr1dTXnmpnQRJzIgJFSt9JgNgorl8bNec6rK4spC2F81UVc7ri8D1SX486DMZAFX9XUVJ/kVMl1OFVGv4ItecL61NWsm+yJcB0h5VNoPNATCaMEUl01J3BQBHhFbjyCubKgWAbgJThDtPnOHL2wSUIsZ0g1VRtfpfkQn4JFWJCXAVmtUnila7naDK4ZuE9nWCdqeW7LxJJ2jqWik9BM8wyPE3PCkMg0aOL0VKVlctb8ghqsV1g1mly2mGIg4xjjYL7lcqdLsrDGZqpxMUyf+uQKInVV0aL0Cf9JxASYRYEyC4sZIQIqxloJ5Eq5tCJtcE+fpCBSypUjkjNFHkGQEjYW5SaTeHPwVhAMIFBM60VJgrxnzPAPDzJfnkSrNdegMIJkKybhT7C4rPfw4BLs/NXISl12Avhsw8nt35bqIYMmWjVYqhuIoUZ7a2mW85bO7vGaGI83VW6aZmgz6e2xhRLHsWP4feVzSsdQOQqiUTIlEXFHUK836vwHyHi2IfEfQRBj0XD7CSwh+AvNyJg2KbK2hx3eElzVtfAMHAGKUcolQDgDdVVTXx0EyKFtTkGwOFO8d1SQt9TKvg+OoA2Gjx+We2/2Y6KAHibq5Mj7nwSBzYVToYwQMStd3x9tDiZZoTRp4PGUFhImC2vjGymdbYqpkh0+uuKvR/2RtklbfOBqQ24Z722AoARhMekMoyL9/2uImlyfzBNVzsE6/V2uM5c3L5FuX/1Z1g1QGFIg6RnV4Rx2e6VNXzNwbA8GYmeXYfDUjMwDYj4J4NWF1ABkCfUPPUhOoaUNYJukdqlyJ7jMBuhQ9QW9TuMTl+eblaS1peMOQIUxCdbPmYnDYoqU9q2fuK1wjD42UbjcOhTF8n+qD39zxoLk/tT7Avs1haa2RrucND/WwwonCCXPb2RRx+eiAN0kEJEh2ta/VVRmVjh1ZhWNrWJDU1RZ0vVGK20LBXdQ3I4rHefTELfY0IHa9x+YAHro0moe7slzdYQFwPgBUIthFYbmIEol96hoeLrYiYe8jGcfMX8RmpdWrPOj5A3Zx9wnzeWdr/DIGYYoGx88VdQnIhFaKJiPbSvWu1vi/h4dp+fQ1wnbDl/38AYMsf6M7Fe9CAO4d4yw+49xrwH2YrCH1f8Ks8AAAAAElFTkSuQmCC'
// tinymce key
export const TinyKey = 'i9loif5tptrktatd5nsxz66i1iqnijfpotc1lqs9mf24za7q'
export const EmptyStatus = <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
// 百度统计
export const baiduGA = 'https://hm.baidu.com/hm.js?c9bbebeb2c76eee718a02b3f5c23f140'
// 百度资源平台验证
export const BaiduSiteVerification = 'codeva-p2DsyF5ySr'

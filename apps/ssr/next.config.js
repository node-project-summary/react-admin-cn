/** @type {import('next').NextConfig} */

const nextConfig = {
  generateBuildId: async () => 'v1',
  trailingSlash: true,
  reactStrictMode: false,
  swcMinify: false,
  distDir: '.out', // 打包后输出目录

  images: {
    domains: ['127.0.0.1:3000', 'localhost', 'react-admin.cn'],
  },

  webpack: (config) => {
    config.module.rules.push({
      test: /\.svg$/i,
      issuer: /\.[jt]sx?$/,
      use: ['@svgr/webpack'],
    })

    return config
  },
}

module.exports = nextConfig

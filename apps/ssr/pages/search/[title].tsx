import { useEffect, useRef, useState } from 'react'
import AppConfig, { EmptyStatus, PageConfig } from '@/config'
import { SearchOutlined } from '@ant-design/icons'
import { Pagination } from '@nextui-org/react'
import CardItem from '@/components/CardItem'
import Layout from '@/layouts/default'
import { fetchArticle } from '@/api'
import Head from 'next/head'
import { Col, Row } from 'antd'
import Router from 'next/router'

type PageProps = {
  articles: any
  total?: number
  keyword?: string
}

const { title } = AppConfig

export default function Search({ articles, total, keyword }: PageProps) {
  const [articleList, setArticleList] = useState(articles)
  const pageRef = useRef<any>(1)
  const pageNum = total ? Math.ceil(total / PageConfig.base.limit) : 0

  useEffect(() => {
    if (articleList?.length) {
      setArticleList(articleList)
    }
  }, [articleList])

  // 分页处理
  const onPaginationChange = (page = 1) => {
    pageRef.current = page
    Router.push({ pathname: '/admin', query: { page } })
  }

  return (
    <Layout>
      <Head>
        <title>{`管理模板 - ${title}`}</title>
      </Head>
      <div className="container app-search-page my-5 relative">
        <div className="app-admin-body">
          <div className="app-search-title mb-5">
            <div className="search-title-txt">
              <h1>
                <SearchOutlined className="mr-2" />搜索到 <i>{ total }</i> 条与 <i>{ keyword }</i> 相关的文章：
              </h1>
            </div>
          </div>
          <Row gutter={30}>
            {articles?.length > 0 && articles.map((item: any) => {
              return (
                <Col key={item.id} className="app-admins-col" lg={6} sm={8} xs={24}>
                  <CardItem data={item} />
                </Col>
              )
            })}
          </Row>
          {!articles?.length && <div className="app-admin-empty">{EmptyStatus}</div>}
        </div>
        <div className="app-page-footer flex justify-between">
          <div />
          <Pagination onChange={onPaginationChange} total={pageNum} />
        </div>
      </div>
    </Layout>
  )
}

export async function getServerSideProps({ query }: any) {
  const page = query?.page || 1
  const keyword = query?.title || ''
  const params = { navId: 1, ...PageConfig.base, page, ...query }

  const { rows: articles, page: { total } } = await fetchArticle({ params })

  return { props: { articles, total, keyword } }
}

import { useEffect, useState } from 'react'
import { LoadingOutlined } from '@ant-design/icons'
import HomeBodyItem from '@/components/Home/BodyItem'
import HomeBodyPart from '@/components/Home/BodyPart'
import HomeBodyTitle from '@/components/Home/BodyTitle'
import Banner from '@/public/images/react-banner.png'
import { Button, Col, Row, Space, Spin } from 'antd'
import Layout from '@/layouts/default'
import { fetchArticle } from '@/api'
import AppConfig from '@/config'
import Image from 'next/image'
import Head from 'next/head'
import Link from 'next/link'

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />

type PostPageProps = {
  adminList: any,
  uiList: any,
  hookList: any,
}

export default function Index({ adminList, uiList, hookList }: PostPageProps) {
  const { title, description, keywords } = AppConfig
  const [loading, setLoading] = useState(true)
  const meta = {
    ui: {
      title: 'UI组件库',
      url: '/ui',
    },
    hooks: {
      title: 'Hooks工具集',
      url: '/hooks',
    },
  }

  useEffect(() => {
    setLoading(false)
  }, [adminList])

  return (
    <Layout>
      <Head>
        <title>{`首页 - ${title}`}</title>
        <meta name="description" content={description} />
        <meta name="keywords" content={keywords} />
      </Head>
      <div className="app-home-hero">
        <div className="container flex">
          <div className="main">
            <p className="text">react管理平台集中地</p>
            <p className="tagline">提交入口</p>
            <div className="actions">
              <Space size={15}>
                <Link href="/post/edit?nav_id=1" target="_blank">
                  <Button type="primary" block>管理后台</Button>
                </Link>
                <Link href="/post/edit?nav_id=2" target="_blank">
                  <Button type="primary" block danger>UI组件库</Button>
                </Link>
                <Link href="/post/edit?nav_id=3" target="_blank">
                  <Button block>Hooks工具集</Button>
                </Link>
              </Space>
            </div>
          </div>
          <div className="image">
            <div className="image-container">
              <Image unoptimized src={Banner} alt="react管理平台集中地-提交入口" width={150} height={130} />
            </div>
          </div>
        </div>
      </div>
      <div className="app-home-body">
        <div className="container relative">
          {loading && (
            <div className="scope-loading">
              <Spin indicator={antIcon} spinning={loading} />
            </div>
          )}
          <div className="app-home-body-item app-home-admin relative">
            <HomeBodyItem adminList={adminList} />
          </div>
          <div className="app-home-body-item app-hooks-admin mt-2 mb-3">
            <Row gutter={30}>
              <Col lg={12} xs={24} sm={8}>
                <HomeBodyTitle data={meta.ui} />
                <HomeBodyPart data={uiList} />
              </Col>
              <Col lg={12} xs={24} sm={8}>
                <HomeBodyTitle data={meta.hooks} />
                <HomeBodyPart data={hookList} />
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export async function getServerSideProps() {
  const { rows: adminList } = await fetchArticle({ params: { navId: 1, page: 1, limit: 4 } })
  const { rows: uiList } = await fetchArticle({ params: { navId: 2, page: 1, limit: 2 } })
  const { rows: hookList } = await fetchArticle({ params: { navId: 3, page: 1, limit: 2 } })

  return {
    props: {
      adminList,
      uiList,
      hookList,
    },
  }
}

import '../styles/tailwind.css'
import 'antd/dist/reset.css'
import '../styles/app.scss'
import 'bytemd/dist/index.css'
import 'juejin-markdown-themes/dist/github.css'
import type { AppProps } from 'next/app'
import { Suspense } from 'react'
import BackToTop from '@/components/BackToTop'
import NextProgress from 'next-progress'
import zhCN from 'antd/locale/zh_CN'
import { ConfigProvider } from 'antd'

const loading = (
  <div className="loader-container">
    <div className="loader" />
  </div>
)

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Suspense fallback={loading}>
      <ConfigProvider locale={zhCN}>
        <NextProgress delay={300} options={{ showSpinner: false }} />
        <Component {...pageProps} />
        <BackToTop />
      </ConfigProvider>
    </Suspense>
  )
}

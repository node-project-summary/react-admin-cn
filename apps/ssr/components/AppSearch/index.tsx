import React from 'react'
import { Form, Input } from 'antd'
import { debounce } from 'lodash-es'
import { useNavId } from '@/hooks/useNavId'

const { Search } = Input

type PageProps = {
  handleSearch: any
}

export default function AppSearch({ handleSearch }: PageProps) {
  const [form] = Form.useForm()
  const { validateFields } = form
  const navId = useNavId()

  // 数据收集
  const onEmit = async () => {
    const values = await validateFields()
    Object.assign(values, { navId })

    handleSearch(values)
  }

  // 搜索
  const onSearch = debounce(() => {
    onEmit()
  }, 500)

  return (
    <Form className="app-header-form" form={form} layout="vertical" name="basic" autoComplete="off" preserve={false} requiredMark={false}>
      <Form.Item name="title" rules={[{ required: true, message: '请输入关键字搜索' }]}>
        <Search placeholder="请输入关键字搜索" onSearch={onSearch} enterButton style={{ width: '20rem' }} />
      </Form.Item>
    </Form>
  )
}

import { useNavId } from '@/hooks/useNavId'
import { Popover } from 'antd'
import { LinkOutlined, PlusOutlined, ShareAltOutlined, WeiboOutlined } from '@ant-design/icons'
import { ThumbsUpFilled } from '@carbon/icons-react'
import { IsBrowser } from '@/components/IsBrowser'
import WeiXin from '@/public/icons/weixin-gray.svg'
import Clipboard from '@/components/Clipboard'
import { isBrowser, shareTo } from '@/utils'
import QQ from '@/public/icons/qq-gray.svg'
import classNames from 'classnames'
import QRCode from 'qrcode.react'
import Link from 'next/link'

type PageProps = {
  isLiked?: boolean
  likeCount: number
  handleLike: any
}

export default function ShareBar({ isLiked, likeCount, handleLike }: PageProps) {
  const navId = useNavId()
  const url = `/post/edit/?nav_id=${navId}`
  const currentUrl = isBrowser() ? window.location.href : ''

  // 点赞
  const onLike = () => {
    likeCount += 1
    handleLike(likeCount)
  }

  // 分享
  const onShare = (type: string) => {
    const options = {
      type,
      url: currentUrl,
      title: document.title,
      pics: '',
      summary: '摘要',
      desc: '描述',
      appkey: 'abcdef',
    }
    shareTo(options)
  }

  // 社交分享
  const overContent = (
    <div className="relative">
      <div className="action-share-popup">
        <ul className="action-share-list">
          <li className="share-item wechat">
            <WeiXin />
            <div className="share-item-title">微信</div>
            <div className="wechat-qrcode animate__animated animate__bounceIn">
              <div className="wechat-qrcode-img">
                <QRCode value={currentUrl} size={115} fgColor="#000000" />
              </div>
              <div className="wechat-qrcode-title">微信扫一扫</div>
            </div>
          </li>
          <li className="share-item qq" onClick={() => onShare('qq')}>
            <QQ />
            <span className="share-item-title">QQ</span>
          </li>
          <li className="share-item weibo" onClick={() => onShare('weibo')}>
            <WeiboOutlined />
            <span className="share-item-title">新浪微博</span>
          </li>
          <li className="share-item link">
            <Clipboard text={currentUrl}>
              <LinkOutlined />
              <span className="share-item-title">文章链接</span>
            </Clipboard>
          </li>
        </ul>
      </div>
    </div>
  )

  return (
    <div className="app-share-bar">
      <div className="action-btn shadow ui-animation">
        <Link href={url} target="_blank" className="ui-link" title="提交你的模板">
          <PlusOutlined />
        </Link>
      </div>
      <div className={classNames('action-btn shadow ui-animation with-badge', { on: isLiked })} data-badge={likeCount} onClick={onLike}>
        <ThumbsUpFilled />
      </div>
      <IsBrowser>
        <Popover placement="rightTop" content={overContent} trigger="hover">
          <div className="action-btn share-more ui-animation shadow">
            <ShareAltOutlined />
          </div>
        </Popover>
      </IsBrowser>
    </div>
  )
}

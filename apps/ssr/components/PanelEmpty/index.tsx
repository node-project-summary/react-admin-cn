import { InfoCircleOutlined } from '@ant-design/icons'

type PageProps = {
  label?: any
}

export default function PanelEmpty({ label = '暂无' }: PageProps) {
  return (
    <div className="panel-empty"><InfoCircleOutlined className="mr-1" />{label}</div>
  )
}

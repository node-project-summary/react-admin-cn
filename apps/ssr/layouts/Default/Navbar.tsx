import { observer } from 'mobx-react-lite'
import AppSearch from '@/components/AppSearch'
import { ROUTES_LINKS } from '@/config'
import Router, { useRouter } from 'next/router'
import Logo from '@/components/Logo'
import Link from 'next/link'

export default observer(function Header() {
  const { pathname } = useRouter()

  // 处理搜索
  const handleSearch = ({ title, navId }: any) => {
    Router.push({ pathname: `/search/${title}`, query: { navId } })
  }

  return (
    <div className="app-header shadow-md">
      <div className="app-header-navbar container white shadow-4 border-bottom pc-model">
        <div className="app-header-main">
          <div className="app-header-logo">
            <span className="app-logo">
              <Logo />
            </span>
          </div>
          <div className="app-header-nav">
            {ROUTES_LINKS.map(({ title, path }: any) => {
              return (
                <Link href={path} key={path} className={pathname === path ? 'active nav-link' : 'nav-link'}> {title} </Link>
              )
            })}
          </div>
          <AppSearch handleSearch={handleSearch} />
        </div>
      </div>
    </div>
  )
})
